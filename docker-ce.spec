%global debug_package %{nil}

# BTRFS is enabled by default, but can be disabled by defining _without_btrfs
%{!?_with_btrfs: %{!?_without_btrfs: %define _with_btrfs 1}}

# binaries and unitfiles are currently called 'docker'
# to match with upstream supplied packages
%global origname docker
%global newname moby
%global service_name %{origname}

# moby / docker-ce / cli
%global git_moby https://github.com/moby/moby
%global commit_moby 4484c46d9d1a2d10b8fc662923ad586daeedb04f
%global shortcommit_moby %(c=%{commit_moby}; echo ${c:0:7})

Name: docker-ce
Version: 20.10.2
Release: 3.cki%{?dist}
Epoch: 3
Source0: engine.tgz
Source1: %{service_name}.service
Source2: %{service_name}.socket
Summary: The open-source application container engine
Group: Tools/Docker
License: ASL 2.0
URL: https://www.docker.com
Vendor: Docker
Packager: Docker <support@docker.com>

Requires: /usr/sbin/groupadd
Requires: docker-ce-cli >= 20.10
Requires: docker-ce-rootless-extras >= 20.10
Requires: container-selinux >= 2:2.74
Requires: libseccomp >= 2.3
Requires: systemd
%if 0%{?rhel} >= 8
Requires: ( iptables or nftables )
%else
Requires: iptables
%endif
Requires: libcgroup
Requires: containerd >= 1.4.1
Requires: tar
Requires: xz

BuildRequires: bash
%{?_with_btrfs:BuildRequires: btrfs-progs-devel}
BuildRequires: ca-certificates
BuildRequires: cmake
BuildRequires: device-mapper-devel
BuildRequires: gcc
BuildRequires: git
BuildRequires: glibc-static
BuildRequires: libseccomp-devel
BuildRequires: libselinux-devel
BuildRequires: libtool
BuildRequires: libtool-ltdl-devel
BuildRequires: make
BuildRequires: pkgconfig
BuildRequires: pkgconfig(systemd)
BuildRequires: selinux-policy-devel
BuildRequires: systemd-devel
BuildRequires: tar
BuildRequires: which
BuildRequires: go-rpm-macros

# conflicting packages
Conflicts: docker
Conflicts: docker-io
Conflicts: docker-engine-cs
Conflicts: docker-ee

# Obsolete packages
Obsoletes: docker-ce-selinux
Obsoletes: docker-engine-selinux
Obsoletes: docker-engine

%description
Docker is a product for you to build, ship and run any application as a
lightweight container.

Docker containers are both hardware-agnostic and platform-agnostic. This means
they can run anywhere, from your laptop to the largest cloud compute instance and
everything in between - and they don't require you to use a particular
language, framework or packaging system. That makes them great building blocks
for deploying and scaling web apps, databases, and backend services without
depending on a particular stack or provider.

%prep
%setup -q -c -n src -a 0

%build

export DOCKER_GITCOMMIT=%{shortcommit_moby}
export DOCKER_DEBUG=1
export GOPATH="${PWD}/go"
export PREFIX="${PWD}"
mkdir -p go/src/github.com/docker
ln -s ${RPM_BUILD_DIR}/src/engine go/src/github.com/docker/docker

pushd ${RPM_BUILD_DIR}/src/engine
for component in tini "proxy dynamic";do
    TMP_GOPATH="${GOPATH}" hack/dockerfile/install/install.sh $component
done
VERSION="${version}" PRODUCT=docker hack/make.sh dynbinary
popd

%check
bundles/dynbinary-daemon/dockerd -v

%install
# install daemon binary
install -D -p -m 0755 $(readlink -f bundles/dynbinary-daemon/dockerd) ${RPM_BUILD_ROOT}%{_bindir}/dockerd

# install proxy
install -D -p -m 0755 docker-proxy ${RPM_BUILD_ROOT}%{_bindir}/docker-proxy

# install tini
install -D -p -m 755 docker-init ${RPM_BUILD_ROOT}%{_bindir}/docker-init

# install systemd scripts
install -D -m 0644 ${RPM_SOURCE_DIR}/docker.service ${RPM_BUILD_ROOT}%{_unitdir}/docker.service
install -D -m 0644 ${RPM_SOURCE_DIR}/docker.socket ${RPM_BUILD_ROOT}%{_unitdir}/docker.socket

%files
%{_bindir}/dockerd
%{_bindir}/docker-proxy
%{_bindir}/docker-init
%{_unitdir}/docker.service
%{_unitdir}/docker.socket

%pre
if ! getent group docker > /dev/null; then
    groupadd --system docker
fi

%post
%systemd_post %{service_name}.service %{service_name}.socket

%preun
%systemd_preun %{service_name}.service %{service_name}.socket

%postun
%systemd_postun_with_restart %{service_name}.service

%changelog
